package com.bender.currencyconverter.data;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currency {

    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("symbol_native")
    @Expose
    private String symbolNative;
    @SerializedName("decimal_digits")
    @Expose
    private int decimalDigits;
    @SerializedName("rounding")
    @Expose
    private double rounding;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name_plural")
    @Expose
    private String namePlural;

    private double rate;

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getSymbolNative() {
        return symbolNative;
    }

    public int getDecimalDigits() {
        return decimalDigits;
    }

    public double getRounding() {
        return rounding;
    }

    public String getCode() {
        return code;
    }

    public String getNamePlural() {
        return namePlural;
    }
}
