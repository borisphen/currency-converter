package com.bender.currencyconverter.data.repository;

import com.bender.currencyconverter.data.RatesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface Api {
    @GET("/latest?base=EUR")
    Observable<RatesResponse> getRates();
}
