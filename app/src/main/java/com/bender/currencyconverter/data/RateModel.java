package com.bender.currencyconverter.data;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;

public class RateModel {
    public final ObservableField<String> token = new ObservableField<>("");
    public final ObservableField<String> title = new ObservableField<>("");
    public final ObservableDouble rate = new ObservableDouble(0.0);
    public final ObservableField<String> amount = new ObservableField<>("0.0");
    public final ObservableBoolean base = new ObservableBoolean();
    public final ObservableField<Drawable> flag = new ObservableField<>();

    @Override
    public String toString() {
        return "RateModel{" +
                "token=" + token.get() +
                ", title=" + title.get() +
                ", rate=" + rate.get() +
                ", amount=" + amount.get() +
                ", base=" + base.get() +
                '}';
    }
}
