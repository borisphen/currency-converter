package com.bender.currencyconverter.data.repository;

import android.content.Context;

import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.data.Currency;
import com.bender.currencyconverter.data.RateModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import utils.JSONUtils;

public class NetworkClient {

    private static final String MOCK_GEO_DATA_FILE = "currencies.json";

    private Api api;
    private Context context;

    @Inject
    public NetworkClient(Application context, Api api) {
        this.context = context;
        this.api = api;
    }

    private Observable<Map<String, Currency>> getDataResponse() {
        String jsonString = JSONUtils.getStringFromAsset(MOCK_GEO_DATA_FILE, context);
        Map<String, Currency> currencyMap = JSONUtils.getMapFromString(jsonString);
        return Observable.just(currencyMap);
    }

    public Observable<Map<String, Currency>> getItems() {
        return api.getRates()
                .subscribeOn(Schedulers.io())
                .map(rates -> {
                    rates.getRates().put(rates.getBase(), 1.0);
                    return rates.getRates();
                })
                .zipWith(getDataResponse(), (rates, currencies) -> {
                    Map<String, Currency> allInfo = new LinkedHashMap<>(rates.size(), 1);
                    for (Map.Entry<String, Double> entry : rates.entrySet()) {
                        String key = entry.getKey();
                        if (currencies.containsKey(key)) {
                            Currency currency = currencies.get(key);
                            currency.setRate(entry.getValue());
                            allInfo.put(key, currency);
                        }

                    }
                    return allInfo;
                });

    }


}
