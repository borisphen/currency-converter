package com.bender.currencyconverter.data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.Map;

public class RatesResponse {

    @SerializedName("base")
    @Expose
    private String base;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("rates")
    @Expose
    private Map<String, Double> rates = new LinkedHashMap<>();

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }


    public Map<String, Double> getRates() {
        return rates;
    }

}