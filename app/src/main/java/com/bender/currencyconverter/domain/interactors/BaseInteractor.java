package com.bender.currencyconverter.domain.interactors;

import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.data.repository.NetworkClient;

import javax.inject.Inject;

public abstract class BaseInteractor {

    protected NetworkClient networkController;

    protected Application context;

    public BaseInteractor(NetworkClient networkController, Application context) {
        this.networkController = networkController;
        this.context = context;
    }
}
