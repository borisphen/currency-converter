package com.bender.currencyconverter.domain.interactors;

import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.data.Currency;
import com.bender.currencyconverter.data.RateModel;
import com.bender.currencyconverter.data.repository.NetworkClient;
import com.bender.currencyconverter.presentation.CurrencyViewModel;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;

@di.scope.PerActivity
public class CurrencyInteractor extends BaseInteractor {

    @Inject
    public CurrencyInteractor(NetworkClient networkController, Application context) {
        super(networkController, context);
    }

    public Observable<Map<String, Currency>> getRates(){
        return networkController.getItems();
    }

}
