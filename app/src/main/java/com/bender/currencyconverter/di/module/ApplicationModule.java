package com.bender.currencyconverter.di.module;


import com.bender.currencyconverter.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application app;

    public ApplicationModule(Application app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Application provideApp(){
        return app;
    }
}
