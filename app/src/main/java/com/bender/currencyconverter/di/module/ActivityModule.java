package com.bender.currencyconverter.di.module;

import com.bender.currencyconverter.presentation.MainActivity;

import dagger.Module;
import dagger.Provides;
import di.scope.PerActivity;

@Module
public class ActivityModule {

    private final MainActivity mapsActivity;

    public ActivityModule(MainActivity mapsActivity) {
        this.mapsActivity = mapsActivity;
    }

    @Provides
    @PerActivity
    public MainActivity provideContext(){
        return mapsActivity;
    }
}
