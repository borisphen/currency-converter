package com.bender.currencyconverter.di.component;

import com.bender.currencyconverter.di.module.ActivityModule;
import com.bender.currencyconverter.presentation.CurrencyViewModel;
import com.bender.currencyconverter.presentation.MainActivity;

import dagger.Subcomponent;
import di.scope.PerActivity;

@PerActivity
@Subcomponent(modules = {ActivityModule.class})
public interface ActivitySubComponent {
    void inject(CurrencyViewModel currencyViewModel);
}
