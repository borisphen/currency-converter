package com.bender.currencyconverter.di.component;


import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.di.module.ActivityModule;
import com.bender.currencyconverter.di.module.ApplicationModule;
import com.bender.currencyconverter.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
    void inject(Application application);
    ActivitySubComponent plus(ActivityModule activityModule);
}
