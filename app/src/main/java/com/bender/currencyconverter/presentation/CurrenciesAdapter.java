package com.bender.currencyconverter.presentation;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bender.currencyconverter.R;
import com.bender.currencyconverter.data.RateModel;
import com.bender.currencyconverter.databinding.RateItemBinding;
import com.bender.currencyconverter.utils.NumberUtils;
import com.bender.currencyconverter.utils.SoftKeyboardUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import utils.CollectionUtils;

public class CurrenciesAdapter extends Adapter<CurrenciesAdapter.ViewHolder> {

    private List<RateModel> models = new ArrayList<>();
    private ItemClicker itemClicker;
    private CurrencyTextWatcher textWatcher;

    CurrenciesAdapter(ItemClicker itemClicker) {
        this.itemClicker = itemClicker;
        textWatcher = new CurrencyTextWatcher();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RateItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.rate_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.dataBinding.setItem(models.get(position));
        EditText etAmount = holder.dataBinding.tvAmount;
        holder.dataBinding.getRoot().setOnClickListener(v -> {
            liftItem(holder.getAdapterPosition());
            etAmount.requestFocus();
            etAmount.post(() -> etAmount.setSelection(etAmount.getText().toString().length()));
            SoftKeyboardUtils.showKeyboard(holder.dataBinding.tvAmount);
        });

        etAmount.setOnFocusChangeListener((view, isFocused) -> {
            if (isFocused) {
                etAmount.setSelection(etAmount.getText().toString().length());
                liftItem(holder.getAdapterPosition());
                etAmount.addTextChangedListener(textWatcher);
            } else {
                etAmount.removeTextChangedListener(textWatcher);
            }
        });

        holder.dataBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public RateItemBinding dataBinding;

        ViewHolder(RateItemBinding dataBinding) {
            super(dataBinding.getRoot());
            this.dataBinding = dataBinding;
        }
    }

    public void setData(List<RateModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    private void liftItem(int position) {
        itemClicker.onItemClick(position);
        models.add(0, models.remove(position));
        notifyItemMoved(position, 0);
    }

    public interface ItemClicker {
        void onItemClick(int position);
    }

    public void recount() {
        RateModel changedRateModel = models.get(0);
        BigDecimal selectedRate = new BigDecimal(changedRateModel.rate.get()).setScale(4, RoundingMode.HALF_UP);
        BigDecimal selectedAmount = new BigDecimal(Double.valueOf(changedRateModel.amount.get().replaceAll(",", "")));
        BigDecimal amountInEuro = selectedAmount.divide(selectedRate, 2, RoundingMode.HALF_UP);

        for (int i = 1; i < models.size(); i++) {
            RateModel currentModel = models.get(i);
            BigDecimal currentRate = new BigDecimal(currentModel.rate.get()).setScale(4, RoundingMode.HALF_UP);
            double amount = amountInEuro.multiply(currentRate).setScale(2, RoundingMode.HALF_UP).doubleValue();
            currentModel.amount.set(NumberUtils.format(amount));
        }
    }


    private class CurrencyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            recount();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}