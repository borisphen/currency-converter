package com.bender.currencyconverter.presentation;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bender.currencyconverter.data.Currency;
import com.bender.currencyconverter.data.RateModel;
import com.bender.currencyconverter.domain.interactors.CurrencyInteractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

public class CurrencyViewModel extends AndroidViewModel {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ArrayList<RateModel> rateModels = new ArrayList<>();
    private MutableLiveData<Boolean> refreshNotifier = new MutableLiveData<>();
    private MutableLiveData<Throwable> errorNotifier = new MutableLiveData<>();

    @Inject
    CurrencyInteractor currencyInteractor;

    public CurrencyViewModel(@NonNull Application application) {
        super(application);
    }

    public void refreshRates() {
        compositeDisposable.add(Observable.interval(1, TimeUnit.SECONDS)
                .flatMap(o -> currencyInteractor.getRates())
                .subscribeWith(new DisposableObserver<Map<String, Currency>>() {
                    @Override
                    public void onNext(Map<String, Currency> rates) {
                        if (utils.CollectionUtils.isEmpty(rateModels)) {
                            createModels(rates);
                            refreshNotifier.postValue(true);
                        } else {
                            updateData(rates);
                            refreshNotifier.postValue(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Error", e.getLocalizedMessage());
                        errorNotifier.postValue(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    private void createModels(Map<String, Currency> rates) {
        for (Map.Entry<String, Currency> entry : rates.entrySet()) {
            String key = entry.getKey();
            Currency currency = rates.get(key);
            RateModel rateModel = new RateModel();
            rateModel.token.set(key);
            rateModel.rate.set(currency.getRate());
            rateModel.title.set(currency.getName());
            int drawableResourceId = getApplication()
                    .getResources().getIdentifier((key).toLowerCase(), "drawable", getApplication().getPackageName());
            if (drawableResourceId > 0) {
                Drawable drawable = getApplication().getResources().getDrawable(drawableResourceId);
                rateModel.flag.set(drawable);
            }

            if (rates.get(key).getRate() == 1.0) {
                rateModel.base.set(true);
                rateModel.amount.set("100.00");
                rateModels.add(0, rateModel);
            } else {
                rateModels.add(rateModel);
            }
        }
    }

    private void updateData(Map<String, Currency> rates) {
        for (RateModel rateModel : rateModels) {
            String token = rateModel.token.get();
            rateModel.rate.set(rates.get(token).getRate());
        }
    }

    public ArrayList<RateModel> getRateModels() {
        return rateModels;
    }

    public MutableLiveData<Boolean> getRefreshNotifier() {
        return refreshNotifier;
    }

    public MutableLiveData<Throwable> getErrorNotifier() {
        return errorNotifier;
    }
}
