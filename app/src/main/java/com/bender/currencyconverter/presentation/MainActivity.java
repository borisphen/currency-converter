package com.bender.currencyconverter.presentation;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.R;
import com.bender.currencyconverter.data.RateModel;
import com.bender.currencyconverter.databinding.ActivityMainBinding;
import com.bender.currencyconverter.di.module.ActivityModule;
import com.bender.currencyconverter.utils.SoftKeyboardUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;

public class MainActivity extends AppCompatActivity {

    private CurrencyViewModel viewModel;
    private CurrenciesAdapter adapter;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(CurrencyViewModel.class);
        Application.getInstance().getApplicationComponent().plus(new ActivityModule(this)).inject(viewModel);
        init();
        subscribeOnData();
    }

    private void init() {
        adapter = new CurrenciesAdapter(position -> binding.rvCurrencies.scrollToPosition(0));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.rvCurrencies.setLayoutManager(layoutManager);
        binding.rvCurrencies.setAdapter(adapter);
        binding.rvCurrencies.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        adapter.setData(viewModel.getRateModels());
    }

    private void subscribeOnData() {
        viewModel.refreshRates();
        viewModel.getRefreshNotifier().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                binding.pbSpinner.setVisibility(View.GONE);
                adapter.recount();
                if (aBoolean) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
//        viewModel.getErrorNotifier().observe(this, e -> Toast.makeText(this, "Oops something goes wrong:\n " + e.getLocalizedMessage(),
//                Toast.LENGTH_LONG).show());
        viewModel.getErrorNotifier().observe(this, e -> {
            binding.pbSpinner.setVisibility(View.GONE);
            showSnack("Oops something goes wrong:\n " + e.getMessage());
        });
    }

    private void showSnack(String text){
        Snackbar.make(binding.rvCurrencies, text, Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        binding.pbSpinner.setVisibility(View.VISIBLE);
                        viewModel.refreshRates();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }
}
