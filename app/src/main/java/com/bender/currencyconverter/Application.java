package com.bender.currencyconverter;

import android.support.multidex.MultiDexApplication;

import com.bender.currencyconverter.di.component.ApplicationComponent;
import com.bender.currencyconverter.di.component.DaggerApplicationComponent;
import com.bender.currencyconverter.di.module.ApplicationModule;
import com.bender.currencyconverter.di.module.NetworkModule;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;


public class Application extends MultiDexApplication {
    private static Application instance;
    private ApplicationComponent applicationComponent;

    @Inject
    Picasso picasso;

    public static Application getInstance() {
        return instance;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        initDependencies();
        super.onCreate();
        instance = this;
        Picasso.setSingletonInstance(picasso);
    }

    private void initDependencies() {
        applicationComponent =
                DaggerApplicationComponent.builder()
                        .applicationModule(new ApplicationModule(this))
                        .networkModule(new NetworkModule())
                        .build();
        applicationComponent.inject(this);
    }

    public Picasso getPicasso() {
        return picasso;
    }
}
