package com.bender.currencyconverter.utils;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * Utility methods to manipulate softInput state
 */
public final class SoftKeyboardUtils {

    /**
     * Hides soft keyboard
     *
     * @param v - view  instance
     */
    public static void hideSoftInput(View v) {
        if (v != null) {
            ((InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static void hideSoftInput(Activity activity) {
        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).
                hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * Toggles soft keyboard
     *
     * @param v - view  instance
     */
    public static void toggleKeyboard(View v) {
        if (v != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    /**
     * Shows soft keyboard
     *
     * @param v - view  instance
     */
    public static void showKeyboard(View v) {
        if (v != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(v, InputMethodManager.SHOW_FORCED);
        }
    }

    /**
     * Force show soft input
     *
     * @param context - app context
     */
    public static void forceShowSoftInput(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private SoftKeyboardUtils() {
    }


    public static void resizeViewSoftInput(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public static void initDefaultViewSoftInput(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }
}
