package utils;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Utility for binding adapters
 */
public class BindingAdaptersUtils {

    @BindingAdapter({"imagePath"})
    public static void loadImage(ImageView view, String imagePath) {
        Picasso.with(view.getContext()).load(imagePath)
                .resize(150, 100)
                .into(view);
//        ImageLoaderUtils.asyncLoadImageIntoImageViewFromURL(view.getContext(), view, imagePath);
    }

    @BindingAdapter("visible")
    public static void setVisibility(View v, boolean isVisible) {
        if (isVisible) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }

    @BindingAdapter({"srcCompat"})
    public static void srcCompat(ImageView view, Drawable drawableResource) {
        view.setImageDrawable(drawableResource);
    }

}
