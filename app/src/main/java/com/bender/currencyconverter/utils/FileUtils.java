package utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * File system helper methods
 */
public final class FileUtils {

    public static String getStringFromAsset(String asset, Context context) {
        try {
            InputStream is = context.getApplicationContext().getAssets().open(asset);
            return readInputStream(is);
        } catch (IOException ex) {
            return null;
        }
    }

    public static String readInputStream(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }
}
