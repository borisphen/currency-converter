package utils;

import android.content.Context;

import com.bender.currencyconverter.data.Currency;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class JSONUtils {

    public static String getStringFromAsset(String asset, Context context) {
        return utils.FileUtils.getStringFromAsset(asset, context);
    }

    public static <T> T getObjectFromAsset(String asset, Class<T> classOfT, Context context) {
        String jsonString = getStringFromAsset(asset, context);
        return getObjectFromString(jsonString, classOfT);
    }

    public static <T> T getObjectFromString(String jsonString, Class<T> classOfT) {
        Gson gson = new Gson();
        T object = gson.fromJson(jsonString, classOfT);
        return object;
    }

    static public String getStringFromObject(Object object) {
        Gson gson = new Gson();
        String jsonRepresentation = gson.toJson(object);
        return jsonRepresentation;
    }

    static public Map<String, Currency> getMapFromString(String jsonString) {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Currency>>(){}.getType();
        return gson.fromJson(jsonString, type);
    }
}
