package utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bender.currencyconverter.Application;
import com.bender.currencyconverter.R;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class ImageLoaderUtils {
    /**
     * Asynchronously loads image from the remote URL to ImageView.
     *
     * @param img   ImageView that will receive loaded image
     * @param url   remote url image source
     * @param color ImageView background color
     */
    public static void asyncLoadImageIntoImageViewFromURL(final Context context, final ImageView img, final String url) {
        if (!TextUtils.isEmpty(url)) {
            try {
                String encodedURL= URLEncoder.encode(url,"UTF-8");
                RequestCreator rc = Application.getInstance().getPicasso().load(encodedURL).transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        if (source.getHeight() == 1 && source.getWidth() == 1){
                            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher_background);
                            source.recycle();
                            return bm;
                        }
                        return source;
                    }

                    @Override
                    public String key() {
                        return url;
                    }
                });
                rc.into(img);
            } catch (UnsupportedEncodingException e) {
            }
        }
    }
}
