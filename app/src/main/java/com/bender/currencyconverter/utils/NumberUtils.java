package com.bender.currencyconverter.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class NumberUtils {
    public static String format(double amount){
        DecimalFormat formatter = new DecimalFormat("0.00");
        return formatter.format(amount);
    }
}
